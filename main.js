const input = document.getElementById("input");
const errorMessage = document.querySelector('.error-message');
const button = document.querySelector('.input-button');

function validateEmail() {
    if (input.value == "") {
        errorMessage.textContent = "Email address can't be empty!"
        input.style.border = "1px solid hsl(354, 100%, 66%)";
        errorMessage.style.display = "block";
    } else {
        if (input.validity.typeMismatch) {
            errorMessage.textContent = "Please provide a valid email address";
            input.style.border = "1px solid hsl(354, 100%, 66%)";
            errorMessage.style.display = "block";
        } else {
            errorMessage.style.display = "none";
            input.style.border = "1px solid hsl(223, 100%, 88%)";
            input.value = "";
        }
    }
}

button.addEventListener('click', () => {
    validateEmail();
});

input.addEventListener('keydown', (e) => {
    console.log(e.keyCode);
    if (e.keyCode == 13) {
        validateEmail();
    }
});